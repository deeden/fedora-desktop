#!/bin/sh


DIR="$(dirname $0)"

HOST=$1
ANSIBLE_USER=$2
shift
shift

export ANSIBLE_HOST_KEY_CHECKING=False

case "$HOST" in
    "local")
        ansible-playbook -i localhost, -c local -K "$DIR"/main.yml --diff $@
         ;;
    *)
        ansible-playbook -i "$HOST", -u "$ANSIBLE_USER" -K "$DIR"/main.yml --diff $@
        ;;
esac
